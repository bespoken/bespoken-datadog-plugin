const _ = require('lodash')
const datadog = require('datadog-metrics')
const path = require('path')

require('dotenv').config()

const enabled = process.env.DATADOG_API_KEY !== undefined
if (!enabled) {
  console.error('Environment variables DATADOG_API_KEY is not set - must be set to send results to datadog')
  process.exit(1)
}

const runName = process.env.DATADOG_RUN_NAME || new Date().toISOString()
const jobName = process.env.DATADOG_JOB_NAME || 'EndToEndTests'
const customer = process.env.DATADOG_CUSTOMER || 'bespoken'

/**
 * Intercepts test results and sends them to DataDog
 * The DATADOG_API_KEY must be set to use this
 */
const DatadogPlugin = {
  onTestEnd: async(test, testResult) => {
    return DatadogPlugin.sendToDataDog(test, testResult)
  },

  onTestSuiteEnd: async(testResults) => {
    return DatadogPlugin.sendSuiteResultsToDataDog(testResults);
  },
  
  sendSuiteResultsToDataDog: async(testResults) => {
    let testSuiteName
    let success = 0
    let error = 0
    let skipped = 0
    for (const element of testResults) {
      if (!testSuiteName) {
        testSuiteName = path.basename(element.test.testSuite.fileName)
      }
      if (element.skipped) {
        skipped++
      } else if (element.passed) {
        success++
      } else {
        error++
      }
    }
    await sendSuiteSuccessRate(testSuiteName, success, error, skipped)
    await flush()
  },

  sendToDataDog: async (test, testResult) => {
    if (!enabled) {
      return
    }

    // If voiceId was not defined in configuration section, set to default voice
    const voiceUsed = _.get(test, 'testSuite.configuration.voiceId', 'Joey')

    // Get the last part of the filename from the test suite - use that as the name
    const testSuiteName = path.basename(test.testSuite.fileName)

    for (const element of testResult._interactionResults) {
      const utterance = _.get(element, 'interaction.utterance')
      let utteranceResult
      if (element._errorOnProcess) {
        utteranceResult = 'error'
      } else {
        utteranceResult = evaluateUtterance(element)
      }

      await sendUtteranceResult(test.description, testSuiteName, utterance, voiceUsed, utteranceResult)
    }

    await sendTestResult(test, testSuiteName, testResult)
  }
}

/**
 * Publishes metrics about an utterance result do DataDog
 * @param {string} testName The name of the test
 * @param {string} testSuite The name of the test suite
 * @param {string} utterance The utterance for the test
 * @param {string} voiceId The voice ID used for the test
 * @param {string} result The result of the test
 */
async function sendUtteranceResult (testName, testSuiteName, utterance, voiceId, result) {
  const tags = [
    `job:${jobName}`,
    `run:${runName}`,
    `customer:${customer}`,
    `test:${testName}`,
    `testSuite:${testSuiteName}`,
    `utterance:${utterance}`,
    `voiceId:${voiceId}`
  ]

  // console.log('UTTERANCE ' + utterance + ' RESULT: ' + result)
  const error = result === 'error' ? 1 : 0
  const success = result === 'passed' ? 1 : 0
  const failure = result === 'failed' ? 1 : 0
  datadog.increment('utterance.error', error, tags)
  datadog.increment('utterance.success', success, tags)
  datadog.increment('utterance.failure', failure, tags)
}

/**
 * Publishes metrics about test results
 * @param {Test} test The test
 * @param {string} testSuiteName The test suite name
 */
async function sendTestResult (test, testSuiteName, testResult) {
  const tags = [
    `job:${jobName}`,
    `run:${runName}`,
    `customer:${customer}`,
    `test:${test.description}`,
    `testSuite:${testSuiteName}`
  ]

  // console.log('TEST ' + test.description + ' RESULT: ' + testResult.passed)
  const skipped = testResult.skipped ? 1 : 0
  const success = testResult.passed ? 1 : 0
  const failure = !testResult.passed ? 1 : 0
  datadog.increment('test.skipped', skipped, tags)
  datadog.increment('test.success', success, tags)
  datadog.increment('test.failure', failure, tags)
}

/**
 * Publishes metrics about test suite success rate
 * @param {string} testSuiteName The test suite name
 * @param {number} success The total number of success tests
 * @param {number} error The total number of error tests
 * @param {number} skipped The total number of skipped tests
 */
async function sendSuiteSuccessRate (testSuiteName, success, error, skipped) {
  const total = success + error + skipped
  if (total === 0) {
    return
  }
  const tags = [
    `job:${jobName}`,
    `run:${runName}`,
    `customer:${customer}`,
    `testSuite:${testSuiteName}`
  ]

  datadog.gauge('testsuite.success_percentage', success / total, tags)
  datadog.gauge('testsuite.failure_percentage', error / total, tags)
  datadog.gauge('testsuite.skipped_percentage', skipped / total, tags)
}

/**
 * Checks whether the utterance has an error
 * @param {string} utterance
 */
function evaluateUtterance (utterance) {
  if (utterance.errors === undefined) {
    return 'passed'
  } else {
    return 'failed'
  }
}

// Wait on the flush to datadog to complete
async function flush () {
  return new Promise((resolve) => {
    datadog.flush(() => {
      resolve()
    }, (e) => {
      console.error('DATADOG FLUSHED WITH ERROR: ' + e)
      resolve()
    })
  })
}

module.exports = DatadogPlugin
